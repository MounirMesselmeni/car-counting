﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SQLite;
using System.IO;
using System.Windows;

namespace SaisieComptageRoutier
{
    class Database
    {
        private SQLiteConnection Connection;
        private String databaseConncetionString;
        private String dataBaseName;

        public Database()
        {
            //String binURL = Environment.CurrentDirectory;
            //String withoutBinURL = binURL.Substring(0, binURL.LastIndexOf("bin"));
            //String databaseURL = withoutBinURL + "resources\\generated\\";
            //String databaseURL = "resources\\generated\\";
            dataBaseName = "bd.s3db";
            databaseConncetionString = "Data Source=resources\\config\\"+ dataBaseName + ";Version=3;";

            /*
            if (File.Exists("resources\\config\\scriptSQL.sql"))
            {
                System.Windows.MessageBox.Show(databaseURL+dataBaseName);
            }
            
            if (!File.Exists(databaseURL + dataBaseName ))
            {
                SQLiteConnection.CreateFile(databaseURL + dataBaseName );
                System.Windows.MessageBox.Show("Database created");
                Connection = new SQLiteConnection(databaseConncetionString);
                Connection.Open();
                //string sql = new TextFileReader().getFileValue(withoutBinURL + "resources\\config\\scriptSQL.sql");
                string sql = new TextFileReader().getFileValue(Properties.Resources.scriptSQL.ToString());
                SQLiteCommand command = new SQLiteCommand(sql, Connection);
                command.ExecuteNonQuery();
            }
             */
                Connection = new SQLiteConnection(databaseConncetionString);
                Connection.Open();
                
        }
        public void CloseConncetion()
        {
            Connection.Close();
        }
       
        public String GetCount(String categoryName, String selectedPeriodFrom )
        {
            string sql = "SELECT Count(*) As count FROM  Period_Category pc,Category c, Period p WHERE "+
                "c.label ='" + categoryName + "' AND pc.category_id=c.id" 
                +" AND p.from_hour ='" + selectedPeriodFrom + "'" + "AND pc.period_id=p.id";
            SQLiteCommand command = new SQLiteCommand(sql, Connection);
            SQLiteDataReader reader = command.ExecuteReader();
            command.Dispose();
            return Convert.ToString(reader["count"]);
        }
        public int GetCategoryIdByLabel(String label) 
        {
            string sql = "SELECT id  FROM  Category c WHERE c.label ='" + label + "';";
            SQLiteCommand command = new SQLiteCommand(sql, Connection);
            SQLiteDataReader reader = command.ExecuteReader();
            return Convert.ToInt32(reader["id"]);

        }
        public int GetPeriodIdByFromHour(String fromHour)
        {
            string sql = "SELECT id FROM  Period p WHERE p.from_hour ='" + fromHour + "';";
            SQLiteCommand command = new SQLiteCommand(sql, Connection);
            SQLiteDataReader reader = command.ExecuteReader();
            return Convert.ToInt32(reader["id"]);

        }
        public void IncrementCount(String categoryName,String selectedPeriodFrom) 
        {
            String sql = "INSERT INTO Period_Category (category_id, period_id) values ( " + GetCategoryIdByLabel(categoryName) + ", "+GetPeriodIdByFromHour(selectedPeriodFrom)+")";
            SQLiteCommand command = new SQLiteCommand(sql, Connection);
            command.ExecuteNonQuery();
            command.Dispose();
        }

        public void DeleteCounts(String selectedPeriodFrom)
        {
            String sql = "DELETE FROM Period_Category  WHERE period_id="+ GetPeriodIdByFromHour(selectedPeriodFrom) + ";";
            SQLiteCommand command = new SQLiteCommand(sql, Connection);
            command.ExecuteNonQuery();
            command.Dispose();
        }

        public Boolean PeriodIsFinished(String selectedPeriodFrom )
        {
        
            string sql = "SELECT p.finished FROM Period p WHERE p.from_hour ='" + selectedPeriodFrom + "'";

            SQLiteCommand command = new SQLiteCommand(sql, Connection);
            SQLiteDataReader reader = command.ExecuteReader();
            command.Dispose();
        
            return (Convert.ToInt32(reader["finished"])==1);
        }

        public void SetPeriodIsFinished(String selectedPeriodFrom, int value)
        {
            string sql = "UPDATE Period SET finished ="+value+" WHERE from_hour ='" + selectedPeriodFrom + "'";
            SQLiteCommand command = new SQLiteCommand(sql, Connection);
            command.ExecuteNonQuery();
            command.Dispose();
        }

        public Boolean HasData(String selectedPeriodFrom)
        {
            string sql = "SELECT Count(*) As count FROM  Period_Category pc, Period p WHERE " +
               " p.from_hour ='" + selectedPeriodFrom + "'" + "AND pc.period_id=p.id";
            
            SQLiteCommand command = new SQLiteCommand(sql, Connection);
            SQLiteDataReader reader = command.ExecuteReader();
            command.Dispose();

            return (Convert.ToInt32(reader["count"]) >0);
        }
        public void ClearDataBase()
        {
            string sql = "DELETE FROM Period_Category;";
            SQLiteCommand command = new SQLiteCommand(sql, Connection);
            command.ExecuteNonQuery();
            command.Dispose();
            SQLiteCommand command2 = new SQLiteCommand(sql, Connection);
            sql = "UPDATE Period set finished = 0 ;";
            command2 = new SQLiteCommand(sql, Connection);
            command2.ExecuteNonQuery();
            command2.Dispose();
        }
    }
}
