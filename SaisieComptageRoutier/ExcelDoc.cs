﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using Excel = Microsoft.Office.Interop.Excel;
using System.Windows;
using System.Reflection;
using System.IO;

namespace SaisieComptageRoutier
{
    class ExcelDoc
    {
        private Excel.Application app = null;

        public Excel.Application App
        {
            get { return app; }
            set { app = value; }
        }
        private Excel.Workbook workbook = null;

        public Excel.Workbook Workbook
        {
            get { return workbook; }
            set { workbook = value; }
        }
        private Excel.Worksheet worksheet = null;

        public Excel.Worksheet Worksheet
        {
            get { return worksheet; }
            set { worksheet = value; }
        }
        private Excel.Range workSheet_range = null;

        public Excel.Range WorkSheet_range
        {
            get { return workSheet_range; }
            set { workSheet_range = value; }
        }
        public ExcelDoc(String path)
        {
            createDoc(path); 
        }
        public void createDoc(String path)
        {
            try
            {       
                app = new Excel.Application();
                /*app.Visible = true;*/
                //workbook = app.Workbooks.Open(path, 0, false, 5, "", "", false, Excel.XlPlatform.xlWindows, "", true, false, 0, true, false, false);
                workbook = app.Workbooks.Open(path);
            }
            catch (Exception e)
            {
                //if not template exist
                workbook = app.Workbooks.Add(1);
            }
            finally
            {
                worksheet = (Excel.Worksheet)workbook.Sheets[1];
            }
        }

        public void addData(int row, int col, string data,
            string cell1, string cell2, string format)
        {
            worksheet.Cells[row, col] = data;
            workSheet_range = worksheet.get_Range(cell1, cell2);
            workSheet_range.NumberFormat = format;
        }

        
        public void releaseObject(object obj)
        {
            try
            {
                System.Runtime.InteropServices.Marshal.ReleaseComObject(obj);
                obj = null;
            }
            catch (Exception ex)
            {
                obj = null;
                MessageBox.Show("Exception Occured while releasing object " + ex.ToString());
            }
            finally
            {
                GC.Collect();
            }
        }
        public bool saveDoc(String path)
        {
            try
            {
                workbook.SaveAs(path, Excel.XlFileFormat.xlWorkbookNormal, Missing.Value, Missing.Value, Missing.Value, Missing.Value, Excel.XlSaveAsAccessMode.xlNoChange, Missing.Value, Missing.Value, Missing.Value, Missing.Value, Missing.Value);
                return true;
            }
            catch (Exception e)
            {
                workbook.Close(false, Missing.Value, Missing.Value);
                app.Quit();

                releaseObject(worksheet);
                releaseObject(workbook);
                releaseObject(app);
                return false;
            }
        }
        public void releaseWorkResources()
        {
            try
            {
                workbook.Close(true, Missing.Value, Missing.Value);
                app.Quit();

                releaseObject(worksheet);
                releaseObject(workbook);
                releaseObject(app);
            }
            catch
            {
            }
            
        }
        
    }
}
