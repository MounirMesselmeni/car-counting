﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace SaisieComptageRoutier
{
    /// <summary>
    /// Interaction logic for Window1.xaml
    /// </summary>
    public partial class CopyProgressWindow : Window
    {
        private delegate void UpdateProgressDelegate(int nPercent);
        private CopyFileExWrapper cfewEngine = new CopyFileExWrapper();

        public CopyProgressWindow()
        {
            InitializeComponent();
        }

        public void copyFiles(string source, string destination)
        {
            try
            {
                List<string> lstSource = new List<string>();
                lstSource.Add(source);
                cfewEngine.EventCopyHandler += new CopyFileExWrapper.CopyEventHandler(cfewEngine_EventCopyHandler);
                cfewEngine.UpdateTextBlock += new CopyFileExWrapper.UpdateTextBlockDelegate(cfewEngine_UpdateTextBlock);
                cfewEngine.CopyFiles(lstSource, destination);
                Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(this,ex.Message,"Erreur de copie");
            }
        }

        private void cfewEngine_UpdateTextBlock(string strMessage)
        {
            txtFile.Text = strMessage;
            txtFile.Refresh();
        }

        private void UpdateProgress(int nPercent)
        {
            pbBar.Value = nPercent;
            pbBar.Refresh();
        }

        private void cfewEngine_EventCopyHandler(CopyFileExWrapper sender, CopyEventArgs e)
        {
            this.Dispatcher.Invoke(new UpdateProgressDelegate(UpdateProgress), new object[] { Convert.ToInt32(e.Percent) });
        }
    }

    public static class ExtensionMethods
    {
        private static Action EmptyDelegate = delegate() { };

        public static void Refresh(this UIElement uiElement)
        {
            uiElement.Dispatcher.Invoke(DispatcherPriority.Render, EmptyDelegate);
        }

        public static void RefreshInput(this UIElement uiElement)
        {
            uiElement.Dispatcher.Invoke(DispatcherPriority.Input, EmptyDelegate);
        }
    }
}