﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;

namespace SaisieComptageRoutier
{
    class XMLManager
    {
        private String nomVideo;

        public String NomVideo
        {
            get { return nomVideo; }
            set { nomVideo = value; }
        }
        private String directionRegionale;

        public String DirectionRegionale
        {
            get { return directionRegionale; }
            set { directionRegionale = value; }
        }
        private String typeRroute;

        public String TypeRroute
        {
            get { return typeRroute; }
            set { typeRroute = value; }
        }
        private String numRroute;

        public String NumRroute
        {
            get { return numRroute; }
            set { numRroute = value; }
        }
        private String sectionComptage;

        public String SectionComptage
        {
            get { return sectionComptage; }
            set { sectionComptage = value; }
        }
        private String posteComptage;

        public String PosteComptage
        {
            get { return posteComptage; }
            set { posteComptage = value; }
        }
        private String dateComptage;

        public String DateComptage
        {
            get { return dateComptage; }
            set { dateComptage = value; }
        }

        private Int32 hours;

        public Int32 Hours
        {
            get { return hours; }
            set { hours = value; }
        }
        private Int32 minutes;

        public Int32 Minutes
        {
            get { return minutes; }
            set { minutes = value; }
        }
        private Int32 seconds;

        public Int32 Seconds
        {
            get { return seconds; }
            set { seconds = value; }
        }

		public XMLManager()
        {
        }

        public void ReadFile(String path)
        {
            XmlTextReader textReader = new XmlTextReader(path);
            while (textReader.Read())
            {
                switch (textReader.NodeType)
                {
                    case XmlNodeType.Element: // The node is an element.
                        if(textReader.Name.Equals("nomVideo"))
                        {
                            textReader.Read();
                            nomVideo = textReader.Value;
                            break;
                        }
                        if (textReader.Name.Equals("directionRegionale"))
                        {
                            textReader.Read();
                            directionRegionale = textReader.Value;
                            break;
                        }
                        if (textReader.Name.Equals("type"))
                        {
                            textReader.Read();
                            typeRroute = textReader.Value;
                            break;
                        }
                        if (textReader.Name.Equals("numero"))
                        {
                            textReader.Read();
                            numRroute = textReader.Value;
                            break;
                        }
                        if (textReader.Name.Equals("sectionComptage"))
                        {
                            textReader.Read();
                            sectionComptage = textReader.Value;
                            break;
                        }
                        if (textReader.Name.Equals("posteComptage"))
                        {
                            textReader.Read();
                            posteComptage = textReader.Value;
                            break;
                        }
                        if (textReader.Name.Equals("jour"))
                        {
                            textReader.Read();
                            dateComptage = textReader.Value+"-";
                            break;
                        }
                        if (textReader.Name.Equals("mois"))
                        {
                            textReader.Read();
                            dateComptage += textReader.Value+"-";
                            break;
                        }
                        if (textReader.Name.Equals("annee"))
                        {
                            textReader.Read();
                            dateComptage += textReader.Value;
                            break;
                        }
                        break;
                }
            }
        }

        public void ReadLastOpenedTime(String path)
        {
            XmlTextReader textReader = new XmlTextReader(path);
            while (textReader.Read())
            {
                switch (textReader.NodeType)
                {
                    case XmlNodeType.Element: // The node is an element.
                        if (textReader.Name.Equals("hours"))
                        {
                            textReader.Read();
                            hours = Convert.ToInt32(textReader.Value);
                            break;
                        }
                        if (textReader.Name.Equals("minutes"))
                        {
                            textReader.Read();
                            minutes = Convert.ToInt32(textReader.Value);
                            break;
                        }
                        if (textReader.Name.Equals("seconds"))
                        {
                            textReader.Read();
                            seconds = Convert.ToInt32(textReader.Value);
                            break;
                        }
                        break;
                }
            }
        }
          public void WriteConfigInformations(String path,String direction, char typeRoute, 
              String numeroRoute, String sectionComptage, String PK, String videoName, DateTime date)
          {

              XmlTextWriter textWriter = new XmlTextWriter(path, null);

              // Opens the document
              textWriter.WriteStartDocument();
              
              // Write first element
              textWriter.WriteStartElement("poste");
              
              textWriter.WriteStartElement("nomVideo");
              textWriter.WriteValue(videoName);
              textWriter.WriteEndElement();

              textWriter.WriteStartElement("directionRegionale");
              textWriter.WriteValue(direction);
              textWriter.WriteEndElement();

              textWriter.WriteStartElement("route");
              textWriter.WriteStartElement("type");
              textWriter.WriteValue(typeRoute.ToString());
              textWriter.WriteEndElement();
              
              textWriter.WriteStartElement("numero");
              textWriter.WriteValue(numeroRoute);
              textWriter.WriteEndElement();

              textWriter.WriteEndElement();

              textWriter.WriteStartElement("sectionComptage");
              textWriter.WriteValue(sectionComptage);
              textWriter.WriteEndElement();

              textWriter.WriteStartElement("posteComptage");
              textWriter.WriteValue(PK);
              textWriter.WriteEndElement();

              textWriter.WriteStartElement("dateComptage");
              textWriter.WriteStartElement("jour");
              textWriter.WriteValue(date.Day);
              textWriter.WriteEndElement();
              textWriter.WriteStartElement("mois");
              textWriter.WriteValue(date.Month);
              textWriter.WriteEndElement();
              textWriter.WriteStartElement("annee");
              textWriter.WriteValue(date.Year);
              textWriter.WriteEndElement();
              textWriter.WriteEndElement();

              // Ends the document.

              textWriter.WriteEndDocument();

              // close writer

              textWriter.Close();
        
          }
        public void WriteLastOpenedTime(String path,int h, int min, int sec )
        {
            XmlTextWriter textWriter = new XmlTextWriter(path, null);

            // Opens the document
            textWriter.WriteStartDocument();

            // Write first element
            textWriter.WriteStartElement("time");
            
            textWriter.WriteStartElement("hours");
            textWriter.WriteValue(h);
            textWriter.WriteEndElement();

            textWriter.WriteStartElement("minutes");
            textWriter.WriteValue(min);
            textWriter.WriteEndElement();

            textWriter.WriteStartElement("seconds");
            textWriter.WriteValue(sec);
            textWriter.WriteEndElement();
            
            textWriter.WriteEndElement();

           
            // Ends the document.

            textWriter.WriteEndDocument();

            // close writer

            textWriter.Close();
        }

    }
}
