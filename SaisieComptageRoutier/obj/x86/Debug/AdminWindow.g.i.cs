﻿#pragma checksum "..\..\..\AdminWindow.xaml" "{406ea660-64cf-4c82-b6f0-42d48172a799}" "B24F8C4417EDC2EB71DA88B131FDF8B5"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.269
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;


namespace SaisieComptageRoutier {
    
    
    /// <summary>
    /// AdminWindow
    /// </summary>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
    public partial class AdminWindow : System.Windows.Window, System.Windows.Markup.IComponentConnector {
        
        
        #line 5 "..\..\..\AdminWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Grid grid;
        
        #line default
        #line hidden
        
        
        #line 6 "..\..\..\AdminWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label lblDirection;
        
        #line default
        #line hidden
        
        
        #line 7 "..\..\..\AdminWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label lblRoute;
        
        #line default
        #line hidden
        
        
        #line 8 "..\..\..\AdminWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label lblTypeRoute;
        
        #line default
        #line hidden
        
        
        #line 9 "..\..\..\AdminWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label lblNumeroRoute;
        
        #line default
        #line hidden
        
        
        #line 10 "..\..\..\AdminWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label lblSectionComptage;
        
        #line default
        #line hidden
        
        
        #line 11 "..\..\..\AdminWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label lblPosteComptage;
        
        #line default
        #line hidden
        
        
        #line 12 "..\..\..\AdminWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label lblDate;
        
        #line default
        #line hidden
        
        
        #line 13 "..\..\..\AdminWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Calendar calDateComptage;
        
        #line default
        #line hidden
        
        
        #line 14 "..\..\..\AdminWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox txtDirection;
        
        #line default
        #line hidden
        
        
        #line 15 "..\..\..\AdminWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox txtNumeroRoute;
        
        #line default
        #line hidden
        
        
        #line 16 "..\..\..\AdminWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox txtSectionComptage;
        
        #line default
        #line hidden
        
        
        #line 17 "..\..\..\AdminWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox txtPK;
        
        #line default
        #line hidden
        
        
        #line 18 "..\..\..\AdminWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label lblVideo;
        
        #line default
        #line hidden
        
        
        #line 19 "..\..\..\AdminWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnChooseVideo;
        
        #line default
        #line hidden
        
        
        #line 20 "..\..\..\AdminWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox txtVideoPath;
        
        #line default
        #line hidden
        
        
        #line 21 "..\..\..\AdminWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnCancel;
        
        #line default
        #line hidden
        
        
        #line 22 "..\..\..\AdminWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnValidate;
        
        #line default
        #line hidden
        
        
        #line 23 "..\..\..\AdminWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ComboBox comboTypeRoute;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/SaisieComptageRoutier;component/adminwindow.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\..\AdminWindow.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            this.grid = ((System.Windows.Controls.Grid)(target));
            return;
            case 2:
            this.lblDirection = ((System.Windows.Controls.Label)(target));
            return;
            case 3:
            this.lblRoute = ((System.Windows.Controls.Label)(target));
            return;
            case 4:
            this.lblTypeRoute = ((System.Windows.Controls.Label)(target));
            return;
            case 5:
            this.lblNumeroRoute = ((System.Windows.Controls.Label)(target));
            return;
            case 6:
            this.lblSectionComptage = ((System.Windows.Controls.Label)(target));
            return;
            case 7:
            this.lblPosteComptage = ((System.Windows.Controls.Label)(target));
            return;
            case 8:
            this.lblDate = ((System.Windows.Controls.Label)(target));
            return;
            case 9:
            this.calDateComptage = ((System.Windows.Controls.Calendar)(target));
            return;
            case 10:
            this.txtDirection = ((System.Windows.Controls.TextBox)(target));
            return;
            case 11:
            this.txtNumeroRoute = ((System.Windows.Controls.TextBox)(target));
            return;
            case 12:
            this.txtSectionComptage = ((System.Windows.Controls.TextBox)(target));
            return;
            case 13:
            this.txtPK = ((System.Windows.Controls.TextBox)(target));
            return;
            case 14:
            this.lblVideo = ((System.Windows.Controls.Label)(target));
            return;
            case 15:
            this.btnChooseVideo = ((System.Windows.Controls.Button)(target));
            
            #line 19 "..\..\..\AdminWindow.xaml"
            this.btnChooseVideo.Click += new System.Windows.RoutedEventHandler(this.btnChooseVideo_Click);
            
            #line default
            #line hidden
            return;
            case 16:
            this.txtVideoPath = ((System.Windows.Controls.TextBox)(target));
            
            #line 20 "..\..\..\AdminWindow.xaml"
            this.txtVideoPath.TextChanged += new System.Windows.Controls.TextChangedEventHandler(this.txtVideoPath_TextChanged);
            
            #line default
            #line hidden
            
            #line 20 "..\..\..\AdminWindow.xaml"
            this.txtVideoPath.MouseDoubleClick += new System.Windows.Input.MouseButtonEventHandler(this.txtVideoPath_MouseDoubleClick);
            
            #line default
            #line hidden
            return;
            case 17:
            this.btnCancel = ((System.Windows.Controls.Button)(target));
            
            #line 21 "..\..\..\AdminWindow.xaml"
            this.btnCancel.Click += new System.Windows.RoutedEventHandler(this.btnCancel_Click);
            
            #line default
            #line hidden
            return;
            case 18:
            this.btnValidate = ((System.Windows.Controls.Button)(target));
            
            #line 22 "..\..\..\AdminWindow.xaml"
            this.btnValidate.Click += new System.Windows.RoutedEventHandler(this.btnValidate_Click);
            
            #line default
            #line hidden
            return;
            case 19:
            this.comboTypeRoute = ((System.Windows.Controls.ComboBox)(target));
            return;
            }
            this._contentLoaded = true;
        }
    }
}

