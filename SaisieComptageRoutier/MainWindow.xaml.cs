﻿using System;
using System.Windows;
using System.Windows.Forms;
using System.Data.SQLite;
using System.IO;
using Excel = Microsoft.Office.Interop.Excel;
using System.Windows.Threading;
using System.Reflection;
using System.Windows.Media.Imaging;
using System.Xml;
using System.Threading;
using Technewlogic.Samples.WpfModalDialog;

namespace SaisieComptageRoutier
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private Database Database;
        private XMLManager configXMLManager;
        private ExcelDoc excelDoc;
        private String configURL;
        private String lastOpenedTimeURL;
        private String videoURL;
        private String dataBaseName;
        private String selectedPeriodFrom;
        private bool IsPlaying;
        DispatcherTimer _timer = new DispatcherTimer();
        private System.Windows.Controls.MenuItem[] menuItems;
        private bool confirm = true;

        public bool Confirm
        {
            get { return confirm; }
            set { confirm = value; }
        }

        /**
         * Constructeur
         * */
        public MainWindow()
        {
            loadWindow();
        }

        /**
         * Load window
         * */
        public void loadWindow()
        {
            /*-----------Get configuration file--------------*/
            loadConfigXML();
            /*-----------Open database connection--------------*/
            Database = new Database();
            /*-----------Initialize Components--------------*/
            InitializeComponent();
            ModalDialog.SetParent(ModalDialogParent);
            InitializeItemsIcons();
            _timer.Interval = TimeSpan.FromMilliseconds(1000);
            _timer.Tick += new EventHandler(ticktock);
            _timer.Start();
            /*-----------Load video in media player--------------*/
            loadVideo();
            loadLastOpenedTimeXML();
            loadVehiclesCounts();
        }
        /**
         * Charger la configuration a partir des fichiers XML
         * */
        private void loadConfigXML()
        {
            configURL = "resources\\config\\config.xml";
            lastOpenedTimeURL = "resources\\config\\lastOpenedTime.xml";
            configXMLManager = new XMLManager();
            configXMLManager.ReadFile(configURL);
            videoURL = "resources\\config\\" + configXMLManager.NomVideo;
            dataBaseName = configXMLManager.DirectionRegionale + "_"
                + configXMLManager.TypeRroute + configXMLManager.NumRroute
                + "_" + configXMLManager.SectionComptage + "_PK"
                + configXMLManager.PosteComptage + "_("
                + configXMLManager.DateComptage + ")";
        }
        /**
         * Charger la vidéo avec la derniere position
         * */
        private void loadLastOpenedTimeXML()
        {
            configXMLManager.ReadLastOpenedTime(lastOpenedTimeURL);
            /*int hours = 0;
            int minutes = 59;
            int seconds = 50;
            */
            int hours = configXMLManager.Hours;
            int minutes = configXMLManager.Minutes;
            int seconds = configXMLManager.Seconds;
            selectedPeriodFrom = "" + (hours + 6);
            goToSeconds(hours * 60 * 60 + minutes * 60 + seconds);
            mediaElement1.Play();
            IsPlaying = true;
            changeActualHourLabel(hours + 6);
        }
        /**
         * Rendre les boutons de parcours de la video invisible si la durée vidéo est moins du bouton
         * */
        private void disableUnusedMenuItems(TimeSpan ts)
        {
            int fromIndex = ts.Minutes == 0 ? ts.Hours : ts.Hours + 1;
            for (int index = fromIndex; index < menuItems.Length; index++)
            {
                menuItems[index].IsEnabled = false;
                menuItems[index].Visibility = System.Windows.Visibility.Hidden;
            }
            int nbHoursFinished = 0;
            for (int index = 0; index < fromIndex; index++)
            {
                int fromHour = extractFromHour(menuItems[index].Name);
                if (Database.PeriodIsFinished("" + fromHour))
                {
                    nbHoursFinished++;
                }
            }
            if (nbHoursFinished == fromIndex)
            {
                converterItem.IsEnabled = true;
            }
        }
        /**
         * Changer la libellé de la période actuelle
         * */
        void changeActualHourLabel(int fromHour)
        {
            int toHour = fromHour + 1;
            if (toHour == 24)
                toHour = 0;
            heureCourante.Content = fromHour + "h - " + toHour + "h";
        }
        /**
         * Incrementation du progressbar et verification si la periode courante est terminée
         * */
        void ticktock(object sender, EventArgs e)
        {
            progressBar.Value = mediaElement1.Position.TotalSeconds;
            verifyTerminatedPeriod(mediaElement1.Position);

        }
        /**
         * Verification si la periode courante est terminée
         * */
        void verifyTerminatedPeriod(TimeSpan time)
        {
            int hours = time.Hours;
            int minutes = time.Minutes;
            int seconds = time.Seconds;
            if (seconds != 59)
                return;
            if (minutes != 59)
                return;
            int toHour = hours + 7;
            int fromHour = toHour - 1;
            mediaElement1.Pause();
            string messageText = "La période (" + fromHour + "H - " + toHour + "H) est terminée !";
            System.Windows.Forms.MessageBox.Show(messageText, "Infos", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Information);
            setPeriodFinished(fromHour);
            goToSeconds((hours + 1) * 60 * 60);
            mediaElement1.Pause();
            heureCourante.Content = "aucune période séléctionnée";
            btnPlay.IsEnabled = false;
            if (mediaElement1.NaturalDuration.HasTimeSpan)
            {
                TimeSpan ts = TimeSpan.FromMilliseconds(mediaElement1.NaturalDuration.TimeSpan.TotalMilliseconds);
                int fromIndex = ts.Minutes == 0 ? ts.Hours : ts.Hours + 1;
                int nbHoursFinished = 0;
                for (int index = 0; index < fromIndex; index++)
                {
                    fromHour = extractFromHour(menuItems[index].Name);
                    if (Database.PeriodIsFinished("" + fromHour))
                    {
                        nbHoursFinished++;
                    }
                }
                if (nbHoursFinished == fromIndex)
                {
                    converterItem.IsEnabled = true;
                }
            }
        }
        /**
         * Chargement de la base du comptage
         * */
        private void loadVehiclesCounts()
        {
            labelA.Content = Convert.ToInt32(Database.GetCount("A", selectedPeriodFrom));
            labelB.Content = Convert.ToInt32(Database.GetCount("B", selectedPeriodFrom));
            labelC.Content = Convert.ToInt32(Database.GetCount("C", selectedPeriodFrom));
            labelD.Content = Convert.ToInt32(Database.GetCount("D", selectedPeriodFrom));
            labelE.Content = Convert.ToInt32(Database.GetCount("E", selectedPeriodFrom));
            labelF1.Content = Convert.ToInt32(Database.GetCount("F1", selectedPeriodFrom));
            labelF2.Content = Convert.ToInt32(Database.GetCount("F2", selectedPeriodFrom));
            labelG1.Content = Convert.ToInt32(Database.GetCount("G1", selectedPeriodFrom));
            labelG2.Content = Convert.ToInt32(Database.GetCount("G2", selectedPeriodFrom));
            labelH.Content = Convert.ToInt32(Database.GetCount("H", selectedPeriodFrom));
            labelI.Content = Convert.ToInt32(Database.GetCount("I", selectedPeriodFrom));
            labelJ.Content = Convert.ToInt32(Database.GetCount("J", selectedPeriodFrom));
        }
        /**
         * Chargement de la vidéo
         * */
        private void loadVideo()
        {
            try { mediaElement1.Source = new Uri(videoURL); }
            catch
            {
                videoURL = Environment.CurrentDirectory + "\\resources\\" + configXMLManager.NomVideo;
                try { mediaElement1.Source = new Uri(videoURL); }
                catch
                {
                    System.Windows.Forms.MessageBox.Show("Problème de chargement de la video !", "Erreur", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    System.Environment.Exit(-1);
                }
            }
        }

        private void btnPlay_Click(object sender, RoutedEventArgs e)
        {
            mediaElement1.Play();
            IsPlaying = true;
        }

        private void btnStop_Click(object sender, RoutedEventArgs e)
        {
            mediaElement1.Stop();
            goToHour(0);
            selectedPeriodFrom = "6";
            loadVehiclesCounts();
            IsPlaying = false;
        }

        private void btnPause_Click(object sender, RoutedEventArgs e)
        {
            mediaElement1.Pause();
            IsPlaying = false;
        }
        /**
         * Evenement declenché lors de l'ouverture de la vidéo
         * */
        private void mediaElement1_MediaOpened(object sender, RoutedEventArgs e)
        {
            if (mediaElement1.NaturalDuration.HasTimeSpan)
            {
                TimeSpan ts = TimeSpan.FromMilliseconds(mediaElement1.NaturalDuration.TimeSpan.TotalMilliseconds);
                progressBar.Minimum = 0;
                progressBar.Maximum = ts.TotalSeconds;
                disableUnusedMenuItems(ts);
            }
        }

        private void btnRewind_Click(object sender, RoutedEventArgs e)
        {
            if (mediaElement1.SpeedRatio > 0.5)
            {
                mediaElement1.SpeedRatio -= 0.25;
                speedValueLabel.Content = mediaElement1.SpeedRatio;
            }
        }

        private void btnFastForward_Click(object sender, RoutedEventArgs e)
        {
            if (mediaElement1.SpeedRatio < 3)
            {
                mediaElement1.SpeedRatio += 0.25;
                speedValueLabel.Content = mediaElement1.SpeedRatio;
            }
        }

        /****************************************************************
         * **************************************************************
         * *************Click sur les boutons des catégories*************
         * **************************************************************
         * **************************************************************/

        private void A_Click(object sender, RoutedEventArgs e)
        {
            if (IsPlaying)
            {
                labelA.Content = Convert.ToInt32(labelA.Content) + 1;
                Database.IncrementCount("A", selectedPeriodFrom);
            }
        }

        private void B_Click(object sender, RoutedEventArgs e)
        {
            if (IsPlaying)
            {
                labelB.Content = Convert.ToInt32(labelB.Content) + 1;
                Database.IncrementCount("B", selectedPeriodFrom);
            }
        }

        private void C_Click(object sender, RoutedEventArgs e)
        {
            if (IsPlaying)
            {
                labelC.Content = Convert.ToInt32(labelC.Content) + 1;
                Database.IncrementCount("C", selectedPeriodFrom);
            }
        }
        private void D_Click(object sender, RoutedEventArgs e)
        {
            if (IsPlaying)
            {
                labelD.Content = Convert.ToInt32(labelD.Content) + 1;
                Database.IncrementCount("D", selectedPeriodFrom);
            }
        }
        private void E_Click(object sender, RoutedEventArgs e)
        {
            if (IsPlaying)
            {
                labelE.Content = Convert.ToInt32(labelE.Content) + 1;
                Database.IncrementCount("E", selectedPeriodFrom);
            }
        }
        private void F1_Click(object sender, RoutedEventArgs e)
        {
            if (IsPlaying)
            {
                labelF1.Content = Convert.ToInt32(labelF1.Content) + 1;
                Database.IncrementCount("F1", selectedPeriodFrom);
            }
        }
        private void F2_Click(object sender, RoutedEventArgs e)
        {
            if (IsPlaying)
            {
                labelF2.Content = Convert.ToInt32(labelF2.Content) + 1;
                Database.IncrementCount("F2", selectedPeriodFrom);
            }
        }
        private void G1_Click(object sender, RoutedEventArgs e)
        {
            if (IsPlaying)
            {
                labelG1.Content = Convert.ToInt32(labelG1.Content) + 1;
                Database.IncrementCount("G1", selectedPeriodFrom);
            }
        }
        private void G2_Click(object sender, RoutedEventArgs e)
        {
            if (IsPlaying)
            {
                labelG2.Content = Convert.ToInt32(labelG2.Content) + 1;
                Database.IncrementCount("G2", selectedPeriodFrom);
            }
        }
        private void H_Click(object sender, RoutedEventArgs e)
        {
            if (IsPlaying)
            {
                labelH.Content = Convert.ToInt32(labelH.Content) + 1;
                Database.IncrementCount("H", selectedPeriodFrom);
            }
        }
        private void I_Click(object sender, RoutedEventArgs e)
        {
            if (IsPlaying)
            {
                labelI.Content = Convert.ToInt32(labelI.Content) + 1;
                Database.IncrementCount("I", selectedPeriodFrom);
            }
        }

        private void J_Click(object sender, RoutedEventArgs e)
        {
            if (IsPlaying)
            {
                labelJ.Content = Convert.ToInt32(labelJ.Content) + 1;
                Database.IncrementCount("J", selectedPeriodFrom);
            }
        }
        /**
         * Evenement de fermeture de l'application
         * */
        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (confirm)
            {
                //Affichage d'un message de confirmation
                DialogResult dResult = System.Windows.Forms.MessageBox.Show("Etes vous sur de vouloir quitter ?", "Confirmation", System.Windows.Forms.MessageBoxButtons.OKCancel, System.Windows.Forms.MessageBoxIcon.Question);
                if (dResult.ToString().Equals("OK"))
                {
                    //Fermeture de la connection
                    Database.CloseConncetion();
                    mediaElement1.Pause();
                    TimeSpan ts = mediaElement1.Position;
                    int seconds = ts.Seconds;
                    int hours = ts.Hours;
                    int minutes = ts.Minutes;
                    //Enregistrement de la position courante dans le fichier xml
                    configXMLManager.WriteLastOpenedTime(lastOpenedTimeURL, hours, minutes, seconds);
                }
                else
                {
                    //Annuler la fermeture
                    e.Cancel = true;
                }
            }
        }
        /**
         * Click sur un bouton de séléction de période
         * */
        private void btn_period_Click(object sender, RoutedEventArgs e)
        {
            System.Windows.Controls.MenuItem clicked = (System.Windows.Controls.MenuItem)sender;
            int fromHour = extractFromHour(clicked.Name);
            int toHour = fromHour + 1;
            int result = 0;
            /*Si une periode est en cours de saisie*/
            if (btnPlay.IsEnabled == true)
            {
                DialogResult dResult = System.Windows.Forms.MessageBox.Show("Toutes les données enregistrées de la période en cours " +
                "\nseront écrasées !\n\nVoulez vous continuer? ", "Alerte",
                System.Windows.Forms.MessageBoxButtons.OKCancel, System.Windows.Forms.MessageBoxIcon.Exclamation);
                if (dResult.ToString().Equals("OK"))
                {
                    result = 1;
                }
            }
            /*Si OK ecraser || Si aucune période en cours*/
            if (result == 1 || btnPlay.IsEnabled == false)
            {
                if (Database.PeriodIsFinished("" + fromHour))
                {
                    DialogResult result2 = System.Windows.Forms.MessageBox.Show("Toutes les données enregistrées de la période ("
                        + fromHour + "H - " + toHour + "H)" +
                    "\nseront écrasées !\n\nVoulez vous continuer? ", "Alerte", System.Windows.Forms.MessageBoxButtons.OKCancel,
                    System.Windows.Forms.MessageBoxIcon.Exclamation);
                    if (!result2.ToString().Equals("OK")) return;
                }
                int index = (fromHour - 6 < 0) ? 18 + fromHour : fromHour - 6;
                Database.DeleteCounts("" + fromHour);
                goToHour(index);
                selectedPeriodFrom = "" + fromHour;
                loadVehiclesCounts();
                changeActualHourLabel(fromHour);
                btnPlay.IsEnabled = true;
                setPeriodNotFinished(fromHour);
                converterItem.IsEnabled = false;
                ModalDialog.persist();
            }
        }
        /**
         * retournée l'heure courant à partir du nom d'un bouton
         * Methode utilisé pour savoir qui declenche l'evenement du click
         * a partir des boutons des periodes
         * */
        private int extractFromHour(String name)
        {
            String number = "";
            for (int index = name.IndexOf("_") + 1; index < name.LastIndexOf("_"); index++)
            {
                number += name[index];
            }
            return Int32.Parse(number);
        }
        /**
         * Positionner la vidéo à une heure donnée
         * */
        private void goToHour(int hour)
        {
            TimeSpan ts = TimeSpan.FromHours(hour);
            mediaElement1.Position = ts;
        }
        /**
         * Positionner la vidéo à une minute donnée
         * */
        private void goToMinutes(int minutes)
        {
            TimeSpan ts = TimeSpan.FromMinutes(minutes);
            mediaElement1.Position = ts;
        }
        /**
         * Positionner la vidéo à une seconde donnée
         * */
        private void goToSeconds(int seconds)
        {
            TimeSpan ts = TimeSpan.FromSeconds(seconds);
            mediaElement1.Position = ts;
        }

        private void MenuItem_Click(object sender, RoutedEventArgs e)
        {
            SaveFileDialog saveDialog = new SaveFileDialog();
            saveDialog.FileName = dataBaseName + ".xls"; // Default file name
            saveDialog.DefaultExt = ".xsl"; // Default file extension
            saveDialog.Filter = "Fichiers excel (*.xsl)|*.xsl ";
            // Show save file dialog box
            DialogResult result = saveDialog.ShowDialog();
            // Process save file dialog box results
            if ("OK".Equals(result.ToString()))
            {
                // Save document
                string filename = saveDialog.FileName;
                if (GenerateExcelFile(filename))
                {
                    System.Windows.Forms.MessageBox.Show("Fichier '" + filename + "' est créé avec succés !", "Info",
                        System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Information);
                }
            }
        }
        /**
         * Generer un fichier excel
         * */
        public bool GenerateExcelFile(String fileName)
        {
            excelDoc = new ExcelDoc(Environment.CurrentDirectory + "\\resources\\config\\Template.xls");
            AddHeadersInfosToExcelFile();
            AddVehiclesNumbers();
            return SaveAndReleaseExcelResources(fileName);

        }
        public void AddHeadersInfosToExcelFile()
        {
            excelDoc.addData(1, 2, configXMLManager.DirectionRegionale, "B1", "B1", "");
            excelDoc.addData(6, 14, configXMLManager.DateComptage, "N6", "N6", "");
            excelDoc.addData(6, 1, "ROUTE : " + configXMLManager.TypeRroute, "A6", "A6", "");
            excelDoc.addData(6, 3, configXMLManager.NumRroute, "C6", "C6", "");
            excelDoc.addData(6, 7, configXMLManager.SectionComptage, "G6", "G6", "");
            excelDoc.addData(6, 11, configXMLManager.PosteComptage, "K6", "K6", "");
        }
        public void AddVehiclesNumbers()
        {
            String[] categories = new string[] { "A", "B", "C", "D", "E", "F1", "F2", "G1", "G2", "H", "I", "J" };
            /*-------Jour---------*/
            for (int line = 11; line <= 26; line++)
            {
                for (int col = 2; col <= 13; col++)
                {
                    excelDoc.addData(line, col, Database.GetCount(categories[col - 2], "" + (line - 5)), "B" + line, "B" + line, "");
                }
            }
            /*-------Nuit---------*/
            for (int line = 34; line <= 41; line++)
            {
                for (int col = 2; col <= 13; col++)
                {
                    excelDoc.addData(line, col, Database.GetCount(categories[col - 2], "" + (line - 12)), "B" + line, "B" + line, "");
                }
            }
        }
        public bool SaveAndReleaseExcelResources(String fileName)
        {
            bool result = excelDoc.saveDoc(fileName);
            excelDoc.releaseWorkResources();
            return result;
        }
        public void InitializeItemsIcons()
        {

            String finished = "Icons\\finished.png";
            String notFinished = "Icons\\notfinished.png";
            String icon;
            menuItems = new System.Windows.Controls.MenuItem[]{item_6_7,item_7_8,item_8_9,item_9_10,item_10_11,item_11_12,item_12_13
                        ,item_13_14,item_14_15,item_15_16,item_16_17,item_17_18,item_18_19,item_19_20,item_20_21,item_21_22,item_22_23
                        ,item_23_0,item_0_1,item_1_2,item_2_3,item_3_4,item_4_5,item_5_6};
            String[] periods = new String[] { "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16"
                                            , "17", "18", "19", "20", "21", "22", "23", "0", "1", "2", "3", "4", "5" };
            for (int index = 0; index < 24; index++)
            {
                if (Database.PeriodIsFinished(periods[index])) icon = finished;
                else icon = notFinished;
                menuItems[index].Icon = new System.Windows.Controls.Image{Source = new BitmapImage(new Uri(icon, UriKind.Relative))};
            }
        }
        /**
         * Marquer une période donnée comme finie
         * */
        public void setPeriodFinished(int fromHour)
        {
            Database.SetPeriodIsFinished("" + fromHour, 1);
            int index = (fromHour - 6 < 0) ? 18 + fromHour : fromHour - 6;
            menuItems[index].Icon = new System.Windows.Controls.Image
            {
                Source = new BitmapImage(new Uri("Icons\\finished.png", UriKind.Relative))
            };
        }
        /**
         * Marquer une période donnée non finie
         * */
        public void setPeriodNotFinished(int fromHour)
        {
            Database.SetPeriodIsFinished("" + fromHour, 0);
            int index = (fromHour - 6 < 0) ? 18 + fromHour : fromHour - 6;

            menuItems[index].Icon = new System.Windows.Controls.Image
            {
                Source = new BitmapImage(new Uri("Icons\\notfinished.png", UriKind.Relative))
            };
        }
        private void adminMenuItemClick(object sender, RoutedEventArgs e)
        {
            new LoginWindow(this).Show();
        }
    }
}
