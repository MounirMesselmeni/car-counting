﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Windows.Forms;
using System.IO;

namespace SaisieComptageRoutier
{
    /// <summary>
    /// Interaction logic for AdminWindow.xaml
    /// </summary>
    public partial class AdminWindow : Window
    {
        private MainWindow mainWindow;
        public AdminWindow(MainWindow main)
        {
            this.mainWindow = main;
            InitializeComponent();
            main.Confirm = false;
            main.Close();
            calDateComptage.DisplayDateEnd = DateTime.Today;
        }

        private void btnChooseVideo_Click(object sender, RoutedEventArgs e)
        {
            openDialog();
        }

        private void openDialog()
        {
            OpenFileDialog fDialog = new OpenFileDialog();
            fDialog.Title = "Choisir la vidéo";
            fDialog.Filter = "MP4 Files|*.mp4;*.avi;*.wmv";
            fDialog.InitialDirectory = @"C:\";
            DialogResult dResult = fDialog.ShowDialog();
            if (dResult.ToString().Equals("OK"))
            {
                txtVideoPath.Text = fDialog.FileName;

            }
        }

        private void btnValidate_Click(object sender, RoutedEventArgs e)
        {
            //Validation des champs de texte
            if (txtDirection.Text.Equals("") || txtNumeroRoute.Text.Equals("") || txtPK.Text.Equals("") || txtSectionComptage.Text.Equals(""))
            {
                System.Windows.Forms.MessageBox.Show("Veuillez remplir les champs vides ", "Erreur", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Error);
                return;
            }

            //Validation vidéo
            if (txtVideoPath.Text.Equals(""))
            {
                System.Windows.Forms.MessageBox.Show("Veuillez choisir une vidéo ", "Erreur", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Error);
                return;
            }
            //Validation date
            if (calDateComptage.SelectedDate == null)
            {
                System.Windows.Forms.MessageBox.Show("Veuillez choisir une date ", "Erreur", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Error);
                return;
            }

            //Ecriture dans le fichier de config XML
            XMLManager configXMLManager = new XMLManager();
            System.Windows.Controls.ComboBoxItem curItem = ((System.Windows.Controls.ComboBoxItem)comboTypeRoute.SelectedItem);
            //Extraction du nom de la vidéo
            String videoName = txtVideoPath.Text.Substring(txtVideoPath.Text.LastIndexOf("\\"));
            videoName = videoName.Replace("\\", "");
            configXMLManager.WriteConfigInformations("resources\\config\\config.xml", txtDirection.Text, curItem.Content.ToString().ElementAt(0),
            txtNumeroRoute.Text, txtSectionComptage.Text, txtPK.Text, videoName, calDateComptage.SelectedDate.Value);
            configXMLManager.WriteLastOpenedTime("resources\\config\\lastOpenedTime.xml", 0, 0, 0);
            //RAZ BDD
            Database db = new Database();
            db.ClearDataBase();
            db.CloseConncetion();
            //Copie du fichier vidéo
            if (File.Exists("resources\\" + videoName))
                File.Delete("resources\\" + videoName);
            CopyProgressWindow CopyProgressWindow = new CopyProgressWindow();
            CopyProgressWindow.Show();
            CopyProgressWindow.copyFiles(txtVideoPath.Text, "resources\\");
            System.Windows.Forms.MessageBox.Show("Configuration réussie ", "Information", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Information);
            Close();

        }

        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void txtVideoPath_TextChanged(object sender, TextChangedEventArgs e)
        {

        }

        private void txtVideoPath_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            openDialog();
        }
    }
}
