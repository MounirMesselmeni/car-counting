CREATE TABLE [Category] (
[id] INTEGER  PRIMARY KEY AUTOINCREMENT NOT NULL,
[label] VARCHAR(2)  NULL,
[designation] VARCHAR(50)  NOT NULL
);

CREATE TABLE [Period] (
[id] integer  PRIMARY KEY AUTOINCREMENT NOT NULL,
[from_hour] VARCHAR(5)  NOT NULL,
[to_hour] VARCHAR(5)  NOT NULL,
[finished] integer  NOT NULL
);

CREATE TABLE [Period_Category] (
[id] INTEGER  PRIMARY KEY AUTOINCREMENT NOT NULL,
[period_id] integer  NOT NULL,
[category_id] integer  NOT NULL,
FOREIGN KEY(category_id) REFERENCES Category(id),
FOREIGN KEY(period_id) REFERENCES Period(id)
);

INSERT INTO Category (id, label, designation) values ( 1, 'A','Cycles' );
INSERT INTO Category (id, label, designation) values ( 2, 'B','Motocycles' );
INSERT INTO Category (id, label, designation) values ( 3, 'C','Voitures particulieres et commerciales' );
INSERT INTO Category (id, label, designation) values ( 4, 'D','Camionnettes cu <1.5t' );
INSERT INTO Category (id, label, designation) values ( 5, 'E','Camions legers cu <3.5t' );
INSERT INTO Category (id, label, designation) values ( 6, 'F1','Camions lourds sans remorque cu < 3.5t' );
INSERT INTO Category (id, label, designation) values ( 7, 'F2','Camions + remorque Tracteur avec semi remorque' );
INSERT INTO Category (id, label, designation) values ( 8, 'G1','Transport exceptionnels' );
INSERT INTO Category (id, label, designation) values ( 9, 'G2','Engins speciaux' );
INSERT INTO Category (id, label, designation) values ( 10, 'H','Tracteurs agricoles avec ou sans remorque' );
INSERT INTO Category (id, label, designation) values ( 11, 'I','Vehicules pour transport en commun' );
INSERT INTO Category (id, label, designation) values ( 12, 'J','Vehicules a traction animale' );

INSERT INTO Period (id,from_hour, to_hour, finished) values (1,6,7, 0 );
INSERT INTO Period (id,from_hour, to_hour, finished) values (2,7,8, 0 );
INSERT INTO Period (id,from_hour, to_hour, finished) values (3,8,9, 0 );
INSERT INTO Period (id,from_hour, to_hour, finished) values (4,9,10, 0 );
INSERT INTO Period (id,from_hour, to_hour, finished) values (5,10,11, 0 );
INSERT INTO Period (id,from_hour, to_hour, finished) values (6,11,12, 0 );
INSERT INTO Period (id,from_hour, to_hour, finished) values (7,12,13, 0 );
INSERT INTO Period (id,from_hour, to_hour, finished) values (8,13,14, 0 );
INSERT INTO Period (id,from_hour, to_hour, finished) values (9,14,15, 0 );
INSERT INTO Period (id,from_hour, to_hour, finished) values (10,15,16, 0 );
INSERT INTO Period (id,from_hour, to_hour, finished) values (11,16,17, 0 );
INSERT INTO Period (id,from_hour, to_hour, finished) values (12,17,18, 0 );
INSERT INTO Period (id,from_hour, to_hour, finished) values (13,18,19, 0 );
INSERT INTO Period (id,from_hour, to_hour, finished) values (14,19,20, 0 );
INSERT INTO Period (id,from_hour, to_hour, finished) values (15,20,21, 0 );
INSERT INTO Period (id,from_hour, to_hour, finished) values (16,21,22, 0 );
INSERT INTO Period (id,from_hour, to_hour, finished) values (17,22,23, 0 );
INSERT INTO Period (id,from_hour, to_hour, finished) values (18,23,0, 0 );
INSERT INTO Period (id,from_hour, to_hour, finished) values (19,0,1, 0 );
INSERT INTO Period (id,from_hour, to_hour, finished) values (20,1,2, 0 );
INSERT INTO Period (id,from_hour, to_hour, finished) values (21,2,3, 0 );
INSERT INTO Period (id,from_hour, to_hour, finished) values (22,3,4, 0 );
INSERT INTO Period (id,from_hour, to_hour, finished) values (23,4,5, 0 );
INSERT INTO Period (id,from_hour, to_hour, finished) values (24,5,6, 0 );
